# Set __version__ in the setup.py 
with open('labjack/version.py') as f: exec(f.read())

from setuptools import setup

setup(name='labjack',
      version=__version__,
      packages=['labjack', 'labjack.t7'],
      install_requires=['pymodbus', 'pyusb==1.0.0a3'],
      url='https://gitlab.com/bendub/labjack',
      author='Benoit Dubois',
      author_email='benoit.dubois@femto-st.fr',
      classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering'],)
