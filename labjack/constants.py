# -*- coding: utf-8 -*-

""" labjack/t7/constants.py """

ORGANIZATION = "FEMTO_Engineering"
APP_BRIEF = "Library dedicated to handle the Labjack DAQ board without LJM lib"
AUTHOR_NAME = "Benoit Dubois"
AUTHOR_MAIL = "benoit.dubois@femto-st.fr"
COPYRIGHT = "FEMTO Engineering"
LICENSE = "GNU GPL v3.0 or upper."

T7_ID = "7"

# Default T7 ethernet parameters
T7_PORT = 502
T7_STREAM_PORT = 702
T7_ETH_TIMEOUT = 0.5  # in s

# Default T7 USB parameters
T7_VID = 0x0CD5
T7_PID = 0x0007
T7_USB_TIMEOUT = 0  # in ms
