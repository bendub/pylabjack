# -*- coding: utf-8 -*-

""" labjack/version.py """
__version__ = "0.1.2"

# 0.1.2 (19/07/2019): Remove need of parameter 'ip' with T7Eth instanciation
#                     and correct connect() method procedure in t7eth.py and
#                     some minor cosmetic change.
# 0.1.1 (14/03/2019): Correct import of t7 in t7eth.py
# 0.1.0 (11/09/2018): Initial version

